package HTL.Imst.spring.data.jba.tutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@AttributeOverrides({
        @AttributeOverride(name = "name", column = @Column(name = "guardien_name")),
        @AttributeOverride(name = "email", column = @Column(name = "guardien_email")),
        @AttributeOverride(name = "mobile", column = @Column(name = "guardien_mobile"))

})
public class Guardien {

    private String name;
    private String email;
    private String mobile;
}
