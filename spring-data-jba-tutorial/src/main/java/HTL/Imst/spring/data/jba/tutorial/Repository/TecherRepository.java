package HTL.Imst.spring.data.jba.tutorial.Repository;

import HTL.Imst.spring.data.jba.tutorial.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TecherRepository extends JpaRepository<Teacher,Long> {
}
