package HTL.Imst.spring.data.jba.tutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataJbaTutorialApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataJbaTutorialApplication.class, args);
	}

}
