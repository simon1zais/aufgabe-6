package HTL.Imst.spring.data.jba.tutorial.Repository;

import HTL.Imst.spring.data.jba.tutorial.entity.Course;
import HTL.Imst.spring.data.jba.tutorial.entity.Student;
import HTL.Imst.spring.data.jba.tutorial.entity.Teacher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.sql.SQLOutput;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CoureseRepositoryTest {

    @Autowired
    private CoureseRepository coureseRepository;

    @Test
    public void printCourses(){
        List<Course> courses = coureseRepository.findAll();

        System.out.println("course = " + courses);
    }

    @Test
    public void saveCourseWithTeacher(){
        Teacher teacher = Teacher.builder()
                .firstName("Sam")
                .lastName("Broun")
                .build();

        Course course = Course.builder()
                .title("python")
                .credit(6)
                .teacher(teacher)
                .build();

        coureseRepository.save(course);
    }

    @Test
    public void findAllPagination(){
        org.springframework.data.domain.Pageable firstPageWithThreeRecords = PageRequest.of(0,3);

        Pageable secondPageWithTwoRecords = PageRequest.of(0,2);

        System.out.printf("C");

        List<Course> courses =
                coureseRepository.findAll(firstPageWithThreeRecords).getContent();

        Long totalElements = coureseRepository.findAll(firstPageWithThreeRecords).getTotalElements();
        Integer totalPages = coureseRepository.findAll(firstPageWithThreeRecords).getTotalPages();

        System.out.println(totalElements);
        System.out.println(totalPages);
        System.out.println(courses);

    }

    @Test
    public void findAllSorting(){

        Pageable sortByTitel = PageRequest.of(0,2, Sort.by("titele"));
        Pageable sortByCreditDesc = PageRequest.of(0,2, Sort.by("credit").descending());
        Pageable sortByTitelAndCreditDesc = PageRequest.of(0,2, Sort.by("titele").descending().and(Sort.by("credit")));

        List<Course> courses = coureseRepository.findAll(sortByTitel).getContent();

        System.out.println(courses);
    }

    @Test
    public void  prindFindByTitleContaining(){

        Pageable findPageTenRecords = PageRequest.of(0,10);

        List<Course> courses = coureseRepository.findByTitleContaining("D",findPageTenRecords).getContent();

        System.out.println(courses);
    }

   @Test
   public void saveCourseWithStudentAndTeacher(){

        Teacher teacher = Teacher.builder()
                .firstName("Lili")
                .lastName("Paris")
                .build();

       Student student = Student.builder()
               .firstName("Lin")
               .lastName("Susan")
               .emailId("ichhabemail@gmail.com")
               .build();

        Course course = Course.builder()
                .title("AI")
                .credit(12)
                .teacher(teacher)
                .build();

        course.addStudents(student);

        coureseRepository.save(course);

   }
}