package HTL.Imst.spring.data.jba.tutorial.Repository;

import HTL.Imst.spring.data.jba.tutorial.entity.Guardien;
import HTL.Imst.spring.data.jba.tutorial.entity.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class StudentRepositoryTest {

    @Autowired
    private StudentRepository studentRepository;

    @Test
    public void saveStudent(){

        Student student = Student.builder()
                .emailId("test@lgmail.com")
                .firstName("Mark")
                .lastName("Musterfrau")
                //.guardianName("Lola")
                //.guardianEmail("Lola@notexist.at")
                //.guardianMobile("9999999999999999999")
                .build();

        studentRepository.save(student);
    }

    @Test
    public void addStudentWithGuardien(){

        Guardien guardien = Guardien.builder()

                .name("Lola")
                .email("Lola@notexist.at")
                .mobile("9999999999999999999")
                .build();

        Student student = Student.builder()
                .emailId("test2@lgmail.com")
                .firstName("Mark")
                .lastName("Musterfrau")
                .guardien(guardien)
                .build();

        studentRepository.save(student);
    }

    @Test
    public void printAllStudent(){

        List<Student> studentList = studentRepository.findAll();

        System.out.println("StudentList = " + studentList);
    }

    @Test
    public void printStudensByFirstName(){

        List<Student> students = studentRepository.findByFirstName("Mark");
        System.out.println(students);

    }

    @Test
    public void printStudensByFirstContainsName(){

        List<Student> students = studentRepository.findByFirstNameContaining("M");
        System.out.println(students);

    }

    @Test
    public void printStudensByGuardienName(){

        List<Student> students = studentRepository.findByGuardienName("Lola");
        System.out.println(students);

    }

    @Test

    public void printStudentByEmailAddress(){

        Student student = studentRepository.getStudentByEmailAddress("test@lgmail.com");
        System.out.println("student = "+student);
    }

    @Test
    public void printStudentFristNameByEmailAddress() {

        String student = studentRepository.getStudentFirstNameByEmailAddress("test@lgmail.com");
        System.out.println("student = " + student);
    }

    @Test
    public void printStudentByEmailAddressNative() {

        Student student = studentRepository.getStudentByEmailAddressNative("test@lgmail.com");
        System.out.println("student = " + student);
    }

    @Test
    public void printStudentByEmailAddressNativeNamedParam() {

        Student student = studentRepository.getStudentByEmailAddressNativeNamedParam("test@lgmail.com");
        System.out.println("student = " + student);

    }

    @Test
    public void updateNameByEmailId() {

       studentRepository.updateStudentNameByEmailId("Tom Tom","test@lgmail.com");
    }
}

