package HTL.Imst.spring.data.jba.tutorial.Repository;

import HTL.Imst.spring.data.jba.tutorial.entity.Course;
import HTL.Imst.spring.data.jba.tutorial.entity.CourseMaterial;
import HTL.Imst.spring.data.jba.tutorial.entity.Teacher;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CorseMaterialRepositoryTest {

    @Autowired
    private CourseMaterialRepository repository;

    @Test
    public void SaveCourceMaterial() {
        Course course = Course.builder()
                .title("DDD")
                .credit(6)
                .build();
        CourseMaterial courseMaterial =
                CourseMaterial.builder()
                        .url("www.nixwissen.at")
                        .course(course)
                        .build();

        repository.save(courseMaterial);
    }

    @Test
    public void printAllCourseMaterials(){

        List<CourseMaterial> courseMaterials = repository.findAll();

        System.out.println("courseMaterials = " + courseMaterials);

    }

}