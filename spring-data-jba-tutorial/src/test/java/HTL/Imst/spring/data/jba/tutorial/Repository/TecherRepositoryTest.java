package HTL.Imst.spring.data.jba.tutorial.Repository;

import HTL.Imst.spring.data.jba.tutorial.entity.Course;
import HTL.Imst.spring.data.jba.tutorial.entity.Teacher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TecherRepositoryTest {

    @Autowired
    private TecherRepository techerRepository;

    @Test
    public void saveTeacher(){
        Course courseDBA = Course.builder()
                .title("DBA")
                .credit(5)
                .build();
        Course courseJava = Course.builder()
                .title("java")
                .credit(6)
                .build();

        Teacher teacher = Teacher.builder()
                .firstName("Tim")
                .lastName("Tomson")
                .course(List.of(courseDBA,courseJava))
                .build();

        techerRepository.save(teacher);
    }
}